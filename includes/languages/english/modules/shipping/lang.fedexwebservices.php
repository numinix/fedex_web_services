<?php
$define = array(
    'MODULE_SHIPPING_FEDEX_WEB_SERVICES_TEXT_TITLE' => 'FedEx',
    'MODULE_SHIPPING_FEDEX_WEB_SERVICES_TEXT_DESCRIPTION' => '<h2>FedEx Web Services</h2><p>You will need to have registered an account with FedEx and proper approval from FedEx identity to use this module. Please see the README.TXT file for other requirements.</p>',
    'MODULE_SHIPPING_FEDEX_WEB_SERVICES_TEXT_DESCRIPTION_SOAP' => '<h2>FedEx Web Services</h2><p><span style="color:#dd0000;">Warning: SOAP Extension is not enabled. FedEx Web Services Shipping Module will not work until SOAP is enabled. Ensure that PHP is compiled with SOAP. Speak to your hosting company if unsure.</span></p> <p>You will need to have registered an account with FedEx and proper approval from FedEx identity to use this module. Please see the README.TXT file for other requirements.</p>',
    'MODULE_SHIPPING_FEDEX_WEB_SERVICES_TEXT_SATURDAY' => 'Saturday Delivery',
    'MODULE_SHIPPING_FEDEX_WEB_SERVICES_SORT_ORDER' => 'Order',
    'MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREE_SHIPPING' => 'Free Shipping',
    'MODULE_SHIPPING_FEDEX_WEB_SERVICES_STATUS' => 'Status',
    'MODULE_SHIPPING_FEDEX_WEB_SERVICES_TAX_CLASS' => 'Tax Class',
    'MODULE_SHIPPING_FEDEX_WEB_SERVICES_ACT_NUM' => 'Fedex Account Number',
    'MODULE_SHIPPING_FEDEX_WEB_SERVICES_METER_NUM' => 'Fedex Meter Number',
);

$zc158 = (PROJECT_VERSION_MAJOR > 1 || (PROJECT_VERSION_MAJOR == 1 && substr(PROJECT_VERSION_MINOR, 0, 3) >= '5.8'));
if ($zc158) {
    return $define;
} else {
    nmx_create_defines($define);
}
// eof